from main_dependency import main_dependency

if __name__ == "__main__":
    user_name: str = input('Username: ')
    main_dependency.get_service_manager().get_chat_parser().parse_chat(user_name)