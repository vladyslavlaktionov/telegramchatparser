import abc
from typing import Union, Optional, List

from telethon.hints import Entity

from modules.telegram_parser.models.message_model import MessageModel
from modules.telegram_parser.models.user_model import UserModel


class ParserModule(abc.ABC):
    @abc.abstractmethod
    def get_chat_member_list(self, chat_id: Union[str, int]) -> List[UserModel]:
        pass

    @abc.abstractmethod
    def get_chat_message_list(self, chat_id: Union[str, int], limit: Optional[int] = None,
                              offset: Optional[int] = None) -> List[MessageModel]:
        pass

    @abc.abstractmethod
    def get_full_message_list(self, chat_id: Union[str, int]) -> List[MessageModel]:
        pass

    @abc.abstractmethod
    def get_entity(self, chat_id: Union[str, int]):
        pass
