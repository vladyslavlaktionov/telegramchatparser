import abc

from modules.config_module_impl.models.config_model import ConfigModel


class ConfigModule(abc.ABC):
    @abc.abstractmethod
    def load_config(self) -> None:
        pass

    @abc.abstractmethod
    def get_config(self) -> ConfigModel:
        pass
