from main_dependency.module_manager import ModuleManager
from main_dependency.service_manager import ServiceManager


class MainDependency:
    _module_manager: ModuleManager
    _service_manager: ServiceManager

    def __init__(self):
        self._module_manager: ModuleManager = ModuleManager()
        self._service_manager: ServiceManager = ServiceManager(
            self._module_manager
        )

    def get_module_manager(self):
        return self._module_manager

    def get_service_manager(self):
        return self._service_manager


