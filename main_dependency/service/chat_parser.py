import abc
from typing import Union


class ChatParser(abc.ABC):
    @abc.abstractmethod
    def parse_chat(self, user_id: Union[str, int]):
        pass
