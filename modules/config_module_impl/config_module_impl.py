import configparser

from main_dependency.modules.config_module import ConfigModule
from modules.config_module_impl.models.config_model import ConfigModel
from modules.config_module_impl.models.parser_config_model import ParserConfigModel


class ConfigModuleImpl(ConfigModule):
    config_path: str
    _config: ConfigModel

    def __init__(self, config_path: str):
        self._config_path = config_path
        self._config_parser = configparser.ConfigParser()
        self.load_config()

    def get_config(self) -> ConfigModel:
        return self._config

    def load_config(self) -> None:
        self._config_parser.read(self._config_path)

        parser_config = ParserConfigModel(
            api_id=int(self._config_parser['TelegramParser']['api_id']),
            api_hash=self._config_parser['TelegramParser']['api_hash'],
            phone=self._config_parser['TelegramParser']['phone']
        )

        self._config = ConfigModel(
            parser_config=parser_config
        )

    def save_config(self, config_name: str, key: str, value: str):
        self._config_parser[config_name][key] = value
        with open(self._config_path, 'w') as configfile:  # save
            self._config_parser.write(configfile)
