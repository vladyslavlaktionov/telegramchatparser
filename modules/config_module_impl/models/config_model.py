from modules.config_module_impl.models.parser_config_model import ParserConfigModel


class ConfigModel:
    _parser_config: ParserConfigModel

    def __init__(self, parser_config: ParserConfigModel):
        self._parser_config = parser_config

    def get_parser_config(self) -> ParserConfigModel:
        return self._parser_config
