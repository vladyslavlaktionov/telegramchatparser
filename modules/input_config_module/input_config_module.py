import configparser

from main_dependency.modules.config_module import ConfigModule
from models.parser_config_bl_model import ParserConfigBLModel
from models.config_bl_model import ConfigBLModel


class InputConfigModule(ConfigModule):
    _config: ConfigBLModel

    def __init__(self):
        self.load_config()

    def get_config(self) -> ConfigBLModel:
        return self._config

    def load_config(self) -> None:
        try:
            parser_config = ParserConfigBLModel(
                api_id=int(input('Telegram api id: ')),
                api_hash=input('Telegram api hash: '),
                phone=input('Telegram account phone: ')
            )
        except ValueError:
            raise Exception('Api id must be integer')
        if not parser_config.phone or parser_config.api_id or parser_config.api_hash:
            raise Exception('Config is empty')

        self._config = ConfigBLModel(
            parser_config=parser_config
        )